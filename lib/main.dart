import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
//    String wordPair = WordPair.random().asPascalCase.toString();
    return MaterialApp(title: 'first flutter app',
      home: RandomWords(),
    );
  }
}

class RandomWords extends StatefulWidget {
  @override
  RandomWordState createState() => new RandomWordState();
}

class RandomWordState extends State<RandomWords> {
  final suggestions = <WordPair>[];
  final textStyle = const TextStyle(fontSize: 18);

  Widget _buildSuggestions() {
    return ListView.builder(
      padding: const EdgeInsets.all(10),
      itemBuilder: (ctx, i) {
        if (i.isOdd) {
          return Divider();
        }
        final realIndex = i~/2;
        if (realIndex >= suggestions.length) {
          suggestions.addAll(generateWordPairs().take(1));
        }
        return _buildRow(suggestions[realIndex]);
      },
    );
  }

  Widget _buildRow(WordPair wp) {
    return ListTile(
      title: Text(
        wp.asPascalCase,
        style: this.textStyle,
      ),
    );
  }

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Yote'),
      ),
      body: _buildSuggestions(),
    );
  }
}